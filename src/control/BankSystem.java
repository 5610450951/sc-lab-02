package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.* ;

import model.BankAccount;
import view.BankGUI;

//มุมคอมเม้น : เป็น instance variable ซึ่งเป็นตัวแปรของคลาสที่สามารถเรียกใช้ได้โดยที่ไม่ต้องอ้างอิงถึงที่อยู่ของข้อมูลนั้น

public class BankSystem {
	
	private static final double DEFAULT_RATE = 5;
	private static final double INITIAL_BALANCE = 1000;
	private JLabel rateLabel;
	private JTextField rateField;
	private JButton button;
	private JLabel resultLabel;
	private JPanel panel;
	private BankAccount account;
	public BankGUI view ;
	public BankSystem system ;
	ActionListener list = new AddInterestListener()  ;
	
	public static void main(String[] args){
		   new BankSystem();
	   }
	
		
	public BankSystem(){
		view = new BankGUI(list);
		account = new BankAccount(INITIAL_BALANCE) ;
	}
	
	
	
	
    class AddInterestListener implements ActionListener
    {
       public void actionPerformed(ActionEvent event)
       {

   		double rate = Double.parseDouble(view.rateField.getText()) ;
   		double interest = account.getBalance() * rate / 100 ;
   		account.deposit(interest) ;
   		view.setResultLebel("balance: " + account.getBalance());
   		
    	   
     
       }            
    }
	      
}
	      
	      
	   


